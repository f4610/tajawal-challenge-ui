# tajawal-challenge-ui

### 1. Install Cypress

[Follow these instructions to install Cypress.](https://on.cypress.io/guides/installing-and-running#section-installing)

### 2. Fork this repo

If you want to experiment with running this project in Continous Integration, you'll need to [fork](https://gitlab.com/f4610/tajawal-challenge-ui.git) it first.

After forking this project in `Gitlab`, run these commands:

```bash
## clone this repo to a local directory
git clone https://gitlab.com/f4610/tajawal-challenge-ui.git challenge-folder

## cd into the cloned repo
cd challenge-folder

## install the node_modules
npm install

## Run against tajawal
npm run cy:run-tajawal 

## Run against almosafer
npm run cy:run-almosafer

```

### 3. Running over schedule

- Go to https://gitlab.com/f4610/tajawal-challenge-ui/-/pipeline_schedules
- Click on "Play" then test will start

### 4. Docker

Docker images will be generated each time developers merge on master

## Test result
