import basePage from "../base/basePage"
import { Given, And, Then } from "cypress-cucumber-preprocessor/steps"
import flight from './flightPage'


Given("I select a random flight origin"), ()=> {
    flight.setRandomOrigin()
}

When("I select a random flight origin test", function () {
    flight.setRandomOrigin()
});

When("I select a random flight destination", function () {
    flight.setRandomDestination()
});

When("I click on search flight", function () {
    flight.clickSearchButtom()
});

Then("I sort by cheapest prices", function () {
    flight.clickSortCheapest()
});

Then("I check if it is sort by cheapest to expensive", function () {
    flight.checkSortSequence()
});

When("I select flight dates randomly", function () {
    flight.setRandomFromDate()
});