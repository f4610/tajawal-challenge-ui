import basePage from "../base/basePage";
import moment from 'moment'

import landingPage from "../landing/landingPage";
const ORIGIN_FIELD = "[data-testid=FlightSearchBox__FromAirportInput]"
const DEST_FIELD = "[data-testid=FlightSearchBox__ToAirportInput]"
const SEARCH_BTN = "[data-testid=FlightSearchBox__SearchButton]"
const CHEAP_SORT_BUTTON = "[data-testid=Cheapest__SortBy]"
const CHEAP_SORT_CHECKED = "[data-testid=Cheapest__SortBy__selected]"
const SEARCH_PROGRESS_BAR = "[data-testid=FlightSearchResults__ProgressBar__loading]"
const FROM_DATE_FLIGHT = "[data-testid=FlightSearchBox__FromDateButton]"
const TO_DATE_FLIGHT = "[data-testid=FlightSearchBox__ToDateButton]"
const DAY_PICK_CALENDAR = "[data-testid=FlightSearchCalendar__"
const FROM_CALENDAR_MONTH = "[data-testid=FlightSearchCalendar__MonthDropdown]"
const FROM_CALENDAR_YEAR = "[data-testid=FlightSearchCalendar__YearDropdown]"

class flightPage extends basePage {

    static setRandomOrigin() {
        let from = ['DXB', 'AUH', 'SHJ', 'JED', 'RUH'];
        let origin = basePage.returnRandomValueFromList(from)
        cy.log(origin)
        cy.get(ORIGIN_FIELD).type(origin+"{enter}")
    }

    static setRandomDestination() {
        let from = ['AMM', 'CAI', 'DEL', 'KHI'];
        let destination = basePage.returnRandomValueFromList(from)
        cy.log(destination)
        cy.get(DEST_FIELD).type(destination+"{enter}")
    }

    static setRandomFromDate() {

        let from = [1, 2, 3, 4, 5, 6, 7, 8, 10]
        let fromDays = basePage.returnRandomValueFromList(from)
        let toDays = basePage.returnRandomValueFromList(from)

        const fromDate = moment().add(fromDays, 'day').toISOString()
        const fromMonth = moment(fromDate).format("MMMM")
        const fromYear = moment(fromDate).format("YYYY")
        const fromCurrent = moment(fromDate).format("YYYY-MM-DD")

        const toDate = moment().add(fromDays + toDays, 'day').toISOString()
        const toMonth = moment(toDate).format("MMMM")
        const toYear = moment(toDate).format("YYYY")
        const toCurrent = moment(toDate).format("YYYY-MM-DD")

        cy.get(FROM_DATE_FLIGHT).click({force: true})
        cy.get(FROM_CALENDAR_MONTH).eq(0).select(fromMonth, {multiple: true})
        cy.get(FROM_CALENDAR_YEAR).eq(0).select(fromYear)
        cy.get(DAY_PICK_CALENDAR + fromCurrent + "]").click()

        cy.get(TO_DATE_FLIGHT).click({force: true})
        cy.get(FROM_CALENDAR_MONTH).eq(0).select(toMonth, {multiple: true})
        cy.get(FROM_CALENDAR_YEAR).eq(0).select(toYear)
        cy.get(DAY_PICK_CALENDAR + toCurrent + "]").click()

    }

    static setRandomToDate() {
        let from = [8, 9, 10, 11, 12, 13]
        let fromDate = basePage.returnRandomValueFromList(from)
        cy.log(fromDate)
        cy.get(TO_DATE_FLIGHT).type(fromDate+"{enter}")
    }

    static clickSearchButtom() {
        cy.get(SEARCH_BTN).click({ multiple: true, force: true})
        cy.get(SEARCH_PROGRESS_BAR).should("be.visible")
    }

    static clickSortCheapest(){
        cy.get(CHEAP_SORT_BUTTON).click({ multiple: true, force: true})
        cy.get(CHEAP_SORT_CHECKED).should('be.visible')
    }

    /**
     * @description checks if the flights are sorted from cheap to costly
     */
    static checkSortSequence() {
        let cellContents = [];
        let current = null
        let previous = null;
        let previousPrice = null;
        return new Cypress.Promise(resolve => {
            cy.get("[data-testid$=PriceLabel]")
                .each(($el, $index) => {

                    if (previousPrice != null){

                        current = parseFloat($el.text().replace(",", "."));
                        previous = parseFloat(previousPrice.replace(",", "."));
                        expect(current).to.be.greaterThan(previous)

                    } else {
                        cy.log("less than two values to compare")
                    }
                    cy.log($el.text())
                    previousPrice = $el.text()
                })
                .then(() => resolve(cellContents));
        });
    }

}
export default flightPage