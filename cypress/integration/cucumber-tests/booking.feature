Feature: booking

  this booking feature
  test the "random" ability to perform some booking

Scenario: booking flight
  Given I go to tajawal main page
    And I check if the current language is english
    And I click on flights
   When I select a random flight origin test
    And I select a random flight destination
    And I select flight dates randomly
    And I click on search flight
   Then I sort by cheapest prices
    And I check if it is sort by cheapest to expensive