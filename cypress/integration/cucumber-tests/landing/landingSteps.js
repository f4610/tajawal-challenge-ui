import basePage from "../base/basePage"
import { Given, And, Then } from "cypress-cucumber-preprocessor/steps"
import landingPage from './landingPage'


Then("I see {string} as result",function(searchItem){
  landingPage.checkSearchValues(searchItem)
})

Given("I go to tajawal main page", function () {
  basePage.goToMainPage()
});

Given("I check if the current language is english", function () {
  landingPage.checkCurrentLanguage()
});

Given("I click on flights", function () {
  landingPage.goToFlightsPage()
});