import basePage from '../base/basePage'
const SEARCH = ':nth-child(6) > .glyphicons'
const SEARCH_INPUT = '#OmniSearchQueryTopNav'
const COMPLETE_SEARCH = 'form > [href="#"] > .glyphicons'
const LANGUAGE = "[data-testid=Header__LanguageSwitch]"
const FLIGHTS_BUTTON = "[data-testid=Header__FlightsNavigationTab]"
const ROUND_TRIP_BUTTON = "[data-testid=FlightSearchBox__RoundTripButton]"

class landingPage extends basePage {

  static checkCurrentLanguage () {

    cy.get(LANGUAGE).then(($lang) => {
      if ($lang.text() === "English") {
        cy.get(LANGUAGE).click()
      }
    })
  }

  static goToFlightsPage (){
    cy.get(FLIGHTS_BUTTON).click()
    cy.get(ROUND_TRIP_BUTTON).should("be.visible")
  }

  static search(input){
    cy.forceClickOnSelector(SEARCH)
    cy.get(SEARCH_INPUT).type(input,{force:true});
    cy.get(SEARCH_INPUT).type('{enter}',{force:true});
    cy.forceClickOnSelector(COMPLETE_SEARCH)
  }

  static checkSearchValues(searchItem){
    cy.selectorContains('div', searchItem);
  }

}
export default landingPage
