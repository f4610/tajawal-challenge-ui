export default class BasePage {
	static setDesktopScreen() {
		cy.setResolution([1980, 1080])
	}

	static goToMainPage() {
		this.setDesktopScreen()
		cy.visit(Cypress.env("URL"))
		cy.log(Cypress.env("URL"))
	}

	/**
	 * @description pick a value within an array and returns it
	 * @param {array} randomList name of the attribute whose request refers to
	 * @returns {string} a string from a list`
	 */
	static returnRandomValueFromList (randomList){
		const random = Math.floor(Math.random() * randomList.length);
		console.log(random, randomList[random]);
		return randomList[random]
	}
}
