// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

//module.exports = (on, config) => {
//    on("file:preprocessor", cucumber());
//  };

// Alternatively you can use CommonJS syntax:
// require('./commands')

//import '../integration/cucumber-tests/developer/developerSteps'
//import '../integration/cucumber-tests//login/loginSteps'
//import '../integration/cucumber-tests//thing/thingSteps'
//import '../integration/cucumber-tests/help/helpSteps'
//import '../integration/cucumber-tests/dashboard/dashboardSteps'
//import '../integration/cucumber-tests/connection/connectionSteps'
//import '../integration/cucumber-tests/navbar/navbarSteps'
//import '../integration/cucumber-tests/menu/menuSteps'
