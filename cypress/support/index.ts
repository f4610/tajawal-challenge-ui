// in cypress/support/index.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to select DOM element by data-cy attribute.
     * @example cy.dataCy('greeting')
     *
     *  In case of new commands, add them in ascending alphabetical order
     */

    /**
     * @description getAttributesThatStartWith return the list of elements whose attribute's selector start with the given prefix
     * @param {string} attrSelector identifier of the attribute to match
     * @param {string} sStart initial value to match for the given attribute
     * @returns the list of elements that starts with @param sStart
     */
    getAttributesThatStartWith( attrSelector:string, sStart:string):  Chainable<Element>

    /**
     * @description getAttributesThatEndWith returns the list of elements whose attribute's selector end with the given prefix
     * @param {string} attrSelector identifier of the attribute to match
     * @param {string} sEnd end value to match for the given attribute
     * @returns the list of elements that end with @param sEnd
     */
    getAttributesThatEndWith( attrSelector:string, sEnd:string):  Chainable<Element>

    /**
     * @description loginToPortalForAutomationTest perform the login on the IOT Platform test moving to the given organization
     * @param {string} iotPortal IOT address portal to use for the login
     * @param {string} name username to use for the login
     * @param {string} pwd password to use for the login
     * @param {string} organization organization to set on login
     * @param {string} loginType to set type of login used e.g "sso" or "regular"
     */
    loginToPortalAndSwitchOrg( iotPortal:string, name:string, pwd:string, organization:string, loginType:string):  Chainable<Element>

    /**
     * @description verifies if the title page matches or contains the given text
     * @param title text to compare
     * @param matchOpt denote if the request is related to an exact match (true) or a contains (false)
     */
    pageTitleText( title:string, matchOpt:boolean ):  Chainable<Element>

    /**
     * @description setResolution set the desktop resolution to a given size
     * @param size denotes the new resolution to set. It could be an array (i.e. [DimPixelX,DimPixelY]) or a predefined identifier (i.e. 'iphone-x')
     */
    setResolution( size: undefined): Chainable<Element>

    /** Custom commands added as shortcut */

    /**
     * @description forceClickOnDropdownValue performs a click on the dropdown value which needs to be selected
     * @param {string} dropdownSelector identifier of the dropdown element whose request refers to
     * @param {string} dropdownValue value of the dropdown element to click
     */
    clickOnDropdownValue( dropdownSelector:string, dropdownValue:string): Chainable<Element>

    /**
     * @description clickOnFirstSelector raise a click on the first selector
     * @param{string} selector identifier of the first matching selector
     */
    clickOnFirstSelector(selector: string): Chainable<Element>

    /**
     * @description clickOnSelector raise a click on the given selector
     * @param{string} selector identifier of the element whose request refers to
     */
    clickOnSelector(selector: string): Chainable<Element>

    /**
     * @description clickOnSelector raise a click on the element with the given text
     * @param {string} text text contained in the element to click
     */
    clickOnText(text: string): Chainable<Element>

    /**
     * @description forceClickOnDropdownValue raise a click on the dropdown value which needs to be selected even when not visible
     * @param {string} dropdownSelector identifier of the dropdown element whose request refers to
     * @param {string} dropdownValue value of the dropdown element to click
     */
    forceClickOnDropdownValue( dropdownSelector:string, dropdownValue:string): Chainable<Element>

    /**
     * @description forceClickOnSelector raise a click on the given selector forcing it even when not visible
     * @param {string} selector identifier of the element whose request refers to
     */
    forceClickOnSelector( selector:string): Chainable<Element>

    /**
     * @description forceClickOnText raise a click on the given text forcing it even when not visible
     * @param {string}  text text contained in the element to click
     */
    forceClickOnText(text: string): Chainable<Element>

    /**
     * @description forceMultipleClickOnSelector raise a multiple click on the given selector forcing it even when not visible
     * @param {string} selector identifier of the element whose request refers to
     */
    forceMultipleClickOnSelector( selector:string): Chainable<Element>

    /**
     * @description forceSelectorChecked raise a check on the given checkbox selector forcing it even when not visible
     * @param {string} selector identifier of the element whose request refers to
     */
    forceSelectorChecked( selector:string): Chainable<Element>

    /**
     * @description forceSelectorUnchecked raise an uncheck on the given checkbox selector forcing it even when not visible
     * @param {string} selector identifier of the element whose request refers to
     */
    forceSelectorUnchecked( selector:string): Chainable<Element>

    /**
     * @description isHidden assertion of the not.exist property on the given selector
     * @param {string} selector
     */
    isHidden(selector:string):  Chainable<Element>

    /**
     * @description isVisible assertion of the be.visible property on the given selector
     * @param {string} selector identifier of the element whose request refers to
     */
    isVisible(selector:string):  Chainable<Element>

    /**
     * @description isNotVisible assertion of the not.be.visible property on the given selector
     * @param {string} selector identifier of the element whose request refers to
     */
    isNotVisible(selector:string):  Chainable<Element>

    /**
     * @description multipleClickOnSelector raise a click on the given selector even when there are multiple matching elements
     * @param {string} selector identifier of the element whose request refers to
     */
    multipleClickOnSelector( selector:string): Chainable<Element>

    /**
     * @description selectorContains verifies if the given selector contains a given text
     * @param {string} selector identifier of the element whose request refers to
     * @param {string} selText value that selector should contains
     */
    selectorContains(selector:string, selText:string):  Chainable<Element>

    /**
     * @description selectorTextMatch verifies if the given selector hase the given text property
     * @param {string} selector identifier of the element whose request refers to
     * @param {string} selText value that selector must match exactly in the text property
     */
    selectorTextMatch(selector:string, selText:string):  Chainable<Element>

    /**
     * @description typeOnSelector types the given text on the given selector
     * @param {string} selector identifier of the element whose request refers to
     * @param {string} inText text to type into the selector
     */
    typeOnSelector(selector:string, inText:string):  Chainable<Element>

    /**
     * @description typeOnSelectorCleaned types the given text on the given selector performing a clean operation before typing
     * @param {string} selector identifier of the element whose request refers to
     * @param {string} inText text to type into the selector
     */
    typeOnSelectorCleaned(selector:string, inText:string):  Chainable<Element>

    /**
     * @description urlInclude verifies if the url includes the given text
     * @param inText text that must be part of the url
     */
    urlInclude( inText:string ):  Chainable<Element>
  }
}
